document.addEventListener("DOMContentLoaded", function(event) {

    var activeElement;

    // var body = document.body;
    const button = document.getElementById("send");
    let parent = null;
    let images = document.querySelector('img');
    let elements = document.querySelectorAll('.lb-editor');
    disableLinks();



    elements.forEach((element) => {
        element.addEventListener('mouseover', () => { elementOver(element) });
        element.addEventListener('mouseout', () => { elementOut(element) });
        element.addEventListener('click', () => { elementClick(element) });
    });

    function elementOver(element){
        element.style.boxShadow = "0 0 6px rgba(35, 173, 278, 1)";
    }

    function elementOut(element){
        if(activeElement != element) {
            element.style.boxShadow = '';
        }
    }

    function elementClick(element) {
            if(activeElement) {
                activeElement.style.boxShadow = '';
            }
        activeElement = element;
        let id = element.id;
        let inputType = element.dataset.inputType;
        window.parent.postMessage({attrId:id, inputType:inputType}, 'http://localhost:4200');
    }

    //dispara depois que todo o conteudo foi carregado, imagens, etc
    window.onload = function() {
        postIframeHeight();
    };

    window.addEventListener(
    "message",
        function (event) {
            // if (event.origin === window.location.origin) {
            if(event.data.inputType == 'image') {
                document.getElementById(event.data.property_id).src = event.data.data;

                images.addEventListener('load', (event) => {
                    postIframeHeight();
                });

            }
            else if(event.data.inputType == 'input') {
                if(event.data.data === null || event.data.data.match(/^ *$/) !== null) {
                    document.getElementById(event.data.property_id).innerText = "...";
                } else {
                    document.getElementById(event.data.property_id).innerText = event.data.data;
                }
                postIframeHeight();
            }
            else if(event.data.inputType == 'textarea') {
                let html;
                html = event.data.data.replace(/(&nbsp;)*/g, ''); //remove &nbsp
                html = html.replace(/(<([^>]+)>)/gi,''); //remove tags html

                if(html === null || html.match(/^ *$/) !== null) {
                    document.getElementById(event.data.property_id).innerHTML = "...";
                } else {
                    document.getElementById(event.data.property_id).innerHTML = event.data.data;
                }
                postIframeHeight();
            }
            else if(event.data.inputType == 'link') {
                if(event.data.subType == 'linkTitle') {
                    document.getElementById(event.data.property_id).innerText = event.data.data;
                }
                else if(event.data.subType == 'linkUrl') {
                    document.getElementById(event.data.property_id).href = event.data.data;
                }
                // console.log("Ahuuuu: " + event.data.data)
                // document.getElementById(event.data.property_id).href = event.data.data.link;
            }

            disableLinks()



            // $("#my-message").attr("src", event.data);
            // }
        },
        false
    );

    function disableLinks() {
        var anchors = document.getElementsByTagName("a");
        for (var i = 0; i < anchors.length; i++) {
            anchors[i].onclick = function() {return false;};
        }
    }

    function postIframeHeight() {
        window.parent.postMessage({iframeHeight: document.body.scrollHeight+100}, 'http://localhost:4200');
    }
});
