<?php

use App\Models\Address;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PlanController;
use App\Http\Controllers\BusinessController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LeadListController;
use App\Http\Controllers\LeadController;
use App\Http\Controllers\LeadListFieldController;
use App\Http\Controllers\LeadListFieldValueController;
use App\Http\Controllers\EmktTemplateController;
use App\Http\Controllers\EmktController;
use App\Http\Controllers\EmktSenderController;
use App\Http\Controllers\LpTemplateController;
use App\Http\Controllers\LpController;
use App\Http\Controllers\FileFolderController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\AutomationController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\ReportController;


//Plans
    Route::middleware('client','throttle:10,1')->post('plan/register', [PlanController::class, 'register']);
    Route::middleware('client','throttle:10,1')->get('plan/{uuid}', [PlanController::class, 'detail']);
    Route::middleware('client','throttle:10,1')->post('plan/update', [PlanController::class, 'update']);
    Route::middleware('client','throttle:10,1')->post('plan/remove', [PlanController::class, 'remove']);
    Route::middleware('client','throttle:50,1')->get('plans', [PlanController::class, 'list']);

//Reports
    Route::middleware('auth:api','throttle:50,1')->get('report/{type}', [ReportController::class, 'get']);

// Business
    Route::middleware('client','throttle:10,1')->post('business/register', [BusinessController::class, 'register']);
    Route::middleware('auth:api','throttle:10,1')->get('business/{uuid}', [BusinessController::class, 'detail']);
    Route::middleware('auth:api','throttle:10,1')->post('business/update', [BusinessController::class, 'update']);
    Route::middleware('auth:api','throttle:10,1')->post('business/remove', [BusinessController::class, 'remove']);
    Route::middleware('client','throttle:30,1')->get('businesses', [BusinessController::class, 'list']);

// User
    Route::middleware('client','throttle:10,1')->post('user/register', [UserController::class, 'register']);
    Route::middleware('throttle:10,1')->post('user/login', [UserController::class, 'login']);
    Route::middleware('auth:api','throttle:10,1')->get('user/{uuid}', [UserController::class, 'detail']);
    Route::middleware('auth:api','throttle:10,1')->post('user/update', [UserController::class, 'update']);
    Route::middleware('auth:api','throttle:10,1')->post('user/remove', [UserController::class, 'remove']);
    Route::middleware('auth:api','throttle:30,1')->get('users', [UserController::class, 'list']);

// Subscription
    Route::middleware('auth:api','throttle:10,1')->post('payment/customer/create', [PaymentController::class, 'createCustomer']);

    Route::middleware('auth:api','throttle:10,1')->post('payment/paymentmethod/add', [PaymentController::class, 'addPaymentMethod']);
    Route::middleware('auth:api','throttle:10,1')->post('payment/paymentmethod/remove', [PaymentController::class, 'removePaymentMethod']);

    Route::middleware('auth:api','throttle:10,1')->post('payment/subscription/create', [PaymentController::class, 'createSubscription']);
    Route::middleware('auth:api','throttle:10,1')->post('payment/subscription/update', [PaymentController::class, 'updateSubscription']);
    Route::middleware('auth:api','throttle:10,1')->post('payment/subscription/cancel', [PaymentController::class, 'cancelSubscription']);

    Route::middleware('auth:api','throttle:10,1')->post('payment/portal/create', [PaymentController::class, 'createportal']);


/**
 * LEADS
 */

//Lead List
    Route::middleware('auth:api','throttle:30,1')->post('lead-list/register', [LeadListController::class, 'register']);
    Route::middleware('auth:api','throttle:30,1')->get('lead-list/{uuid}', [LeadListController::class, 'detail']);
    Route::middleware('auth:api','throttle:30,1')->post('lead-list/update', [LeadListController::class, 'update']);
    Route::middleware('auth:api','throttle:30,1')->post('lead-list/remove', [LeadListController::class, 'remove']);
    Route::middleware('auth:api','throttle:30,1')->get('lead-lists', [LeadListController::class, 'list']);

//Lead
    Route::middleware('client','throttle:30,1')->post('lead/register', [LeadController::class, 'register']);
    Route::middleware('auth:api','throttle:10,1')->post('lead/import', [LeadController::class, 'import']);
    Route::middleware('auth:api','throttle:30,1')->post('lead/remove', [LeadController::class, 'remove']);
    Route::middleware('auth:api','throttle:30,1')->post('lead/remove-selected', [LeadController::class, 'removeSelected']);
    Route::middleware('auth:api','throttle:30,1')->get('leads/{lead_list_uuid}', [LeadController::class, 'list']);

//Lead List Field
    Route::middleware('auth:api','throttle:10,1')->post('lead-list-field/register', [LeadListFieldController::class, 'register']);
    Route::middleware('auth:api','throttle:10,1')->post('lead-list-field/update', [LeadListFieldController::class, 'update']);
    Route::middleware('auth:api','throttle:10,1')->post('lead-list-field/remove', [LeadListFieldController::class, 'remove']);
    Route::middleware('auth:api','throttle:30,1')->get('lead-list-fields/{lead_list_uuid}', [LeadListFieldController::class, 'list']);

//Lead List Field Value
    Route::middleware('auth:api')->post('lead-list-field-value/register', [LeadListFieldValueController::class, 'register']); //será necessário?


/**
 * E-MAILS MARKETING
 */

 //Template
    Route::middleware('client','throttle:10,1')->post('emkt-template/register', [EmktTemplateController::class, 'register']);
    Route::middleware('client','throttle:10,1')->get('emkt-template/{uuid}', [EmktTemplateController::class, 'detail']);
    Route::middleware('client','throttle:10,1')->post('emkt-template/update', [EmktTemplateController::class, 'update']);
    Route::middleware('client','throttle:10,1')->post('emkt-template/remove', [EmktTemplateController::class, 'remove']);
    Route::middleware('auth:api','throttle:30,1')->get('emkt-templates', [EmktTemplateController::class, 'list']);
    Route::middleware('throttle:30,1')->get('emkt-template/constructor/{emkt_template_uuid}', [EmktTemplateController::class, 'constructor']);

//Sender
    Route::middleware('auth:api','throttle:30,1')->post('emkt-sender/register', [EmktSenderController::class, 'register']);
    Route::middleware('throttle:10,1')->get('emkt-sender/validate/{email}/{token}', [EmktSenderController::class, 'validade']);
    Route::middleware('auth:api','throttle:30,1')->get('emkt-sender/{uuid}', [EmktSenderController::class, 'detail']);
    Route::middleware('auth:api','throttle:30,1')->post('emkt-sender/update', [EmktSenderController::class, 'update']);
    Route::middleware('auth:api','throttle:30,1')->post('emkt-sender/remove', [EmktSenderController::class, 'remove']);
    Route::middleware('auth:api','throttle:30,1')->get('emkt-senders', [EmktSenderController::class, 'list']);

//Email
    Route::middleware('auth:api','throttle:10,1')->post('emkt/register', [EmktController::class, 'register']);
    Route::middleware('auth:api','throttle:30,1')->get('emkt/{uuid}', [EmktController::class, 'detail']);
    Route::middleware('auth:api','throttle:30,1')->post('emkt/update', [EmktController::class, 'update']);
    Route::middleware('auth:api','throttle:30,1')->post('emkt/remove', [EmktController::class, 'remove']);
    Route::middleware('auth:api','throttle:30,1')->get('emkts', [EmktController::class, 'list']);
    Route::middleware('auth:api','throttle:30,1')->get('emkt/constructor/{emkt_uuid}', [EmktController::class, 'constructor']);
    Route::middleware('client')->get('emkt/schedule/process', [EmktController::class, 'process']);
    Route::middleware('throttle:30,1')->get('emkt/open/register/{emkt_uuid}/{lead_uuid}', [EmktController::class, 'open']);
    Route::middleware('throttle:30,1')->get('emkt/click/register/{emkt_uuid}/{lead_uuid}/{link}', [EmktController::class, 'click'])->where('link', '.*');
    Route::middleware('throttle:30,1')->get('emkt/unsubscribe/remove/{lead_uuid}', [EmktController::class, 'unsubscribe']);

/**
 * LANDING PAGES
 */

 //Template
    Route::middleware('client','throttle:10,1')->post('lp-template/register', [LpTemplateController::class, 'register']);
    Route::middleware('client','throttle:10,1')->get('lp-template/{uuid}', [LpTemplateController::class, 'detail']);
    Route::middleware('client','throttle:10,1')->post('lp-template/update', [LpTemplateController::class, 'update']);
    Route::middleware('client','throttle:10,1')->post('lp-template/remove', [LpTemplateController::class, 'remove']);
    Route::middleware('client','throttle:30,1')->get('lp-templates', [LpTemplateController::class, 'list']);

//Landing Page
    Route::middleware('auth:api','throttle:10,1')->post('lp/register', [LpController::class, 'register']);
    Route::middleware('auth:api','throttle:30,1')->get('lp/{uuid}', [LpController::class, 'detail']);
    Route::middleware('auth:api','throttle:30,1')->post('lp/update', [LpController::class, 'update']);
    Route::middleware('auth:api','throttle:30,1')->post('lp/remove', [LpController::class, 'remove']);
    Route::middleware('auth:api','throttle:30,1')->get('lps', [LpController::class, 'list']);
    Route::middleware('auth:api','throttle:30,1')->get('lp/constructor/{lp_uuid}', [LpController::class, 'constructor']);



/**
 * FILES
 */

//Folder
    Route::middleware('auth:api','throttle:30,1')->post('file-folder/register', [FileFolderController::class, 'register']);
    Route::middleware('auth:api','throttle:30,1')->get('file-folder/{uuid}', [FileFolderController::class, 'detail']);
    Route::middleware('auth:api','throttle:30,1')->post('file-folder/update', [FileFolderController::class, 'update']);
    Route::middleware('auth:api','throttle:30,1')->post('file-folder/remove', [FileFolderController::class, 'remove']);
    Route::middleware('auth:api','throttle:30,1')->get('file-folders', [FileFolderController::class, 'list']);

//File
    Route::middleware('auth:api','throttle:30,1')->post('file/upload', [FileController::class, 'upload']);
    Route::middleware('auth:api','throttle:30,1')->get('file/{uuid}', [FileController::class, 'detail']);
    Route::middleware('auth:api','throttle:30,1')->post('file/update', [FileController::class, 'update']);
    Route::middleware('auth:api','throttle:30,1')->post('file/remove', [FileController::class, 'remove']);
    Route::middleware('auth:api','throttle:30,1')->get('files/{filed_folder_uuid?}', [FileController::class, 'list']);
    Route::middleware('auth:api','throttle:30,1')->get('file/get/{type}/{file}', [FileController::class, 'getFile']);


/**
 * AUTOMATIONS
 */

 //Automation
    Route::middleware('auth:api','throttle:10,1')->post('automation/register', [AutomationController::class, 'register']);
    Route::middleware('auth:api','throttle:30,1')->get('automation/{uuid}', [AutomationController::class, 'detail']);
    Route::middleware('auth:api','throttle:30,1')->post('automation/update', [AutomationController::class, 'update']);
    Route::middleware('auth:api','throttle:30,1')->post('automation/remove', [AutomationController::class, 'remove']);
    Route::middleware('auth:api','throttle:30,1')->get('automations', [AutomationController::class, 'list']);
    Route::middleware('client')->get('automation/schedule/proccess', [AutomationController::class, 'proccess']);
