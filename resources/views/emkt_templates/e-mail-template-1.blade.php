<!DOCTYPE html>
<html>
<head>
<title>Page Title</title>
<link rel="stylesheet" href="{{url('lb-editor/styles.css')}}"/>
</head>
<body>

<img id="logo" data-input-type="image" class="lb-editor" src="https://via.placeholder.com/150" />
<h1 id="title" data-input-type="input" class="lb-editor">My First Heading</h1>
<p id="paragraph" data-input-type="textarea" class="lb-editor">My first paragraph.</p>
<img id="quemsomos" data-input-type="image" class="lb-editor" src="https://via.placeholder.com/150" /><br>
<a id="link1" data-input-type="link" class="lb-editor" href="#" target="_blank">Aqui vai o link</a><br><br>
<a id="link2" data-input-type="link" class="lb-editor" href="#" target="_blank">Outro link</a>

{{-- <button id="send">Send</button> --}}

</body>
<script src="{{url('lb-editor/scripts.js')}}"></script>
</html>
