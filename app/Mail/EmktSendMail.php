<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmktSendMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $emkt;
    protected $lead;

    public function __construct($emkt, $lead)
    {
        $this->emkt = $emkt;
        $this->lead = $lead;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emkt_templates.'.$this->emkt->emkt_template_slug)
                    ->from($this->emkt->emkt_sender_email, $this->emkt->emkt_sender_title)
                    ->subject($this->emkt->emkt_subject)
                    ->with([
                        'emkt' => $this->emkt,
                        'lead' => $this->lead
                    ]);;
    }
}
