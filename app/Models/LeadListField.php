<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeadListField extends Model
{
    use HasFactory;

    protected $hidden = ['id'];

    protected $fillable = [
        'uuid',
        'title',
        'type',
        'options',
        'required',
        'lead_list_id',
        'user_id',
        'business_id',
    ];
}
