<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeadList extends Model
{
    use HasFactory;

    protected $hidden = ['id'];

    protected $fillable = [
        'uuid',
        'title',
        'user_id',
        'business_id'
    ];

    public function children() {
        return $this->hasMany(LeadListField::class);
    }
}
