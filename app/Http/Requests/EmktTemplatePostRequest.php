<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EmktTemplatePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:5|max:45|regex:/^[a-zA-ZÀ-ÿ0-9-\s]+$/',
            'slug' => [
                'required',
                Rule::unique('emkt_templates','slug')->ignore($this->input('uuid'), 'uuid'),
                'min:5',
                'regex:/^[a-z0-9-\s]+$/',
            ],
            'thumb' => [
                $this->input('uuid') != null ? '' : 'required',
                'file',
                'mimes:jpg,png'
            ],
        ];
    }
}
