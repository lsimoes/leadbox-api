<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BusinessPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:45|regex:/^[a-zA-ZÀ-ÿ0-9-&.\s]+$/',
            'shortname' => 'required|min:3|max:45|regex:/^[a-z0-9-\s]+$/',
            'docnumber' => 'required|min:5|max:45|regex:/^[a-zA-Z0-9-.\/s]+$/',
            'address' => 'required|min:5|max:170|regex:/^[a-zA-ZÀ-ÿ0-9-&.\s]+$/',
            'country' => 'required|min:2|max:2|regex:/^[A-Z\s]+$/',
            'state' => 'required|min:2|max:45|regex:/^[a-zA-ZÀ-ÿ0-9-&.\s]+$/',
            'city' => 'required|min:2|max:45|regex:/^[a-zA-ZÀ-ÿ0-9-&.\s]+$/',
            'postalcode' => 'required|min:5|max:45|regex:/^[a-zA-Z0-9-\s]+$/',
            'timezone' => 'required|min:5|max:45|regex:/^[a-zA-Z0-9_\\\s]+$/',
            'status' => 'required|numeric|max:1',
            'plan_id' => 'required|numeric|max:10'
        ];
    }
}
