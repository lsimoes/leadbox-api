<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FilePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => [
                $this->input('uuid') == null ? '' : 'required',
                'min:2',
                'max:45',
                'regex:/^[a-zA-ZÀ-ÿ0-9-\s]+$/'
            ],
            'files[]' => [
                // $this->input('uuid') != null ? '' : 'required',
                'file',
                'mimes:jpg,jpeg,png,gif,svg,pdf,doc,docx,xls,xlsx,ppt,pptx',
                'max:10240' //10mb
            ],
        ];
    }
}
