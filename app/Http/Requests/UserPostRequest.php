<?php

namespace App\Http\Requests;

// use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:5|max:100|regex:/^[a-zA-ZÀ-ÿ\s]+$/',
            'email' => [
                'required', 'email:rfc,dns', Rule::unique('users','email')->ignore($this->input('uuid'), 'uuid'),
            ],
            'password' => ['min:8','max:16', $this->input('uuid') != null ? '' : 'required']
        ];
    }


    // public function failedValidation(Validator $validator)
    // {
    //     $errors = $validator->errors(); // Here is your array of errors
    //     dd($errors);
    // }
}
