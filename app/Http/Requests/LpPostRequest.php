<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LpPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:5|max:100|string',
            'slug' => 'required|min:5|max:100|string',
            'attributes' => 'required',
            'status' => 'required|numeric',
            'lp_template_uuid' => 'required|string',
            'lead_list_uuid' => 'required|string',
            'template_attributes' => 'array|max:150'
        ];
    }
}
