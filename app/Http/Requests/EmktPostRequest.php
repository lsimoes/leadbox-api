<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmktPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subject' => 'required|min:5|max:100|string',
            'type' => 'required|numeric',
            'schedule' => [$this->input('action') == 'send' ? 'required' : ''],
            'attributes' => 'required',
            'emkt_sender_uuid' => 'required|string',
            'emkt_template_uuid' => 'required|string',
            'template_attributes' => 'array',
            'lead_lists_uuids' => [
                $this->input('uuid') != null ? '' : 'required',
                'array'
            ]
        ];
    }
}
