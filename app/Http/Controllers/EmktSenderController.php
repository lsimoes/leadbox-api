<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\EmktSenderPostRequest;

use Notification;
use App\Notifications\EmktSenderValidation;

use App\Models\User;

class EmktSenderController extends Controller
{
    public function register(EmktSenderPostRequest $request) {

        $validation_uuid = Str::uuid();

        $emkt_sender = DB::table('emkt_senders')->insert([
            'uuid' => Str::uuid(),
            'title' => $request['title'],
            'email' => $request['email'],
            'status' => 0,
            'validation_token' => Hash::make($validation_uuid),
            'user_id' => auth()->user()->id,
            'business_id' => auth()->user()->business_id,
        ]);

        $data = [
            'email' => $request['email'],
            'validation_uuid' => $validation_uuid
        ];

        Notification::route('mail', [
            $request['email'] => $request['title'],
        ])->notify(new EmktSenderValidation($data));
    }

    public function validade($email, $validation_uuid) {

        $emkt_sender = DB::table('emkt_senders')
                ->select('validation_token')
                ->where('email', $email)
                ->first();

        if (Hash::check($validation_uuid, $emkt_sender->validation_token)) {
            $emkt_sender = DB::table('emkt_senders')
                ->where('email', $email)
                ->update([
                    'status' => 1,
                    'validation_token' => null,
                ]);

            $content = 'E-mail validado com sucesso!';
            return view('messages', compact('content'));
        }
        else {
            $content = 'Não foi possível fazer esta validação!';
            return view('messages', compact('content'));
        }

    }

    public function list() {
        $emkt_senders = DB::table('emkt_senders')
                ->select('uuid', 'title','email','status')
                ->where('business_id', auth()->user()->business_id)
                ->where('status','!=',2)
                ->get();
        return $emkt_senders;
    }

    public function remove(Request $request) {
        $emkt_sender = DB::table('emkt_senders')
              ->where('uuid', $request['uuid'])
              ->where('business_id', auth()->user()->business_id)
              ->update([
                    'status' => 2 //2 = removido
                ]);
    }

    public function detail($uuid) {
        $emkt_sender = DB::table('emkt_senders')
                ->select('uuid', 'title','email','status')
                ->where('business_id', auth()->user()->business_id)
                ->where('uuid',$uuid)
                ->get();
        return $emkt_sender;
    }

    public function update(EmktSenderPostRequest $request) {
        $emkt_sender = DB::table('emkt_senders')
            ->where('uuid', $request['uuid'])
            ->where('business_id', auth()->user()->business_id)
            ->update([
                    'title' => $request['title']
                ]);
    }
}
