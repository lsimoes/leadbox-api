<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Http\Requests\EmktPostRequest;
use App\Models\Emkt;
use App\Models\Lead;
use App\Models\EmktTemplate;
use App\Models\EmktSender;
use App\Models\LeadList;
use App\Mail\EmktSendMail;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use Response;
use File;

class EmktController extends Controller
{
    public function register(EmktPostRequest $request) {
        $emkt = DB::table('emkts')->insertGetId([
            'uuid' => Str::uuid(),
            'subject' => $request['subject'],
            'type' => $request['type'],
            'attributes' => json_encode($request['attributes']),
            'schedule' => $request['schedule'],
            'status' => $request['action'] == 'save' ? 0 : 1,
            'emkt_template_id' => EmktTemplate::where('uuid', $request['emkt_template_uuid'])->firstOrFail()['id'],
            'emkt_sender_id' => EmktSender::where('uuid', $request['emkt_sender_uuid'])->firstOrFail()['id'],
            'user_id' => auth()->user()->id,
            'business_id' => auth()->user()->business_id,
        ]);

        foreach($request['lead_lists_uuids'] as $lead_list_uuid) {
            $emkt_lead_list = DB::table('emkt_lead_lists')->insert([
                'uuid' => Str::uuid(),
                'lead_list_id' => LeadList::where('uuid', $lead_list_uuid)->firstOrFail()['id'],
                'emkt_id' => $emkt
            ]);
        }

        if($request['action'] == 'send') {
            $emkt_schedule = DB::table('emkt_schedules')->insert([
                'uuid' => Str::uuid(),
                'schedule' => $request['schedule'],
                'emkt_id' => $emkt
            ]);
        }
    }

    public function detail($uuid) {
        $emkt = DB::table('emkts')
                ->join('emkt_templates', 'emkts.emkt_template_id', '=', 'emkt_templates.id')
                ->join('emkt_senders', 'emkts.emkt_sender_id', '=', 'emkt_senders.id')
                ->select('emkts.uuid as uuid',
                        'emkts.subject',
                        'emkts.type',
                        'emkts.schedule',
                        'emkts.status',
                        'emkt_templates.uuid as emkt_template_uuid',
                        'emkt_senders.uuid as emkt_sender_uuid')
                ->where('emkts.uuid', $uuid)
                ->where('emkts.business_id', auth()->user()->business_id)
                ->first();
        return $emkt;
    }

    public function constructor($emkt_uuid) {

        $emkt = Emkt::where('uuid', $emkt_uuid)->firstOrFail();

        $emk_template = DB::table('emkt_templates')
                ->select('slug')
                ->where('id', Emkt::where('uuid', $emkt_uuid)->firstOrFail()['emkt_template_id'])
                ->first();
        $constructor = true;
        return view('emkt_templates.'.$emk_template->slug, compact('emkt','constructor'));
        //criar os blades dos templates, definir local dos templates no AppServiceProvider
    }

    public function update(EmktPostRequest $request) {

        $emkt = DB::table('emkts')
              ->where('uuid', $request['uuid'])
              ->where('business_id', auth()->user()->business_id)
              ->update([
                    'subject' => $request['subject'],
                    'type' => $request['type'],
                    'attributes' => $request['attributes'],
                    'schedule' => $request['schedule'],
                    'status' => $request['status'],
                    'emkt_sender_id' => EmktSender::where('uuid', $request['emkt_sender_uuid'])->firstOrFail()['id'],
                    'user_id' => auth()->user()->id,
                ]);

        if($request->has('lead_lists')) {
            $emkt_lead_list = DB::table('emkt_lead_lists')
                ->where('emkt_id', Emkt::where('uuid', $request['uuid'])->firstOrFail()['id'])
                ->delete();

            foreach($request['lead_lists_uuids'] as $lead_list_uuid) {
                $emkt_lead_list = DB::table('emkt_lead_lists')->insert([
                    'uuid' => Str::uuid(),
                    'lead_list_id' => LeadList::where('uuid', $lead_list_uuid)->firstOrFail()['id'],
                    'emkt_id' => Emkt::where('uuid', $request['uuid'])->firstOrFail()['id']
                ]);
            }
        }
    }

    public function remove(Request $request) {
        $emkt = DB::table('emkts')
                ->where('uuid', $request['uuid'])
                ->where('business_id', auth()->user()->business_id)
                ->delete();
    }

    public function list() {
        $emkts = DB::table('emkts')
                ->select('uuid', 'subject','type','schedule','status')
                ->where('business_id', auth()->user()->business_id)
                ->get();
        return $emkts;
    }

    public function process(Request $request) {

        $request->ip() == env('ALLOWED_IP_API_CRON') ?: abort( response('no-permission', 403) ); //if not permission, abort

        $recipients = array();

        $now = Carbon::now('UTC')->format("Y-m-d H:i");

        $emkt_schedules = DB::table('emkt_schedules')
                ->where('schedule', '<=', $now)
                ->get();

        //Listando registros do Schedule
        foreach ($emkt_schedules as $emkt_schedule) {
            //Dados dos emails a serem enviados
            $emkt = DB::table('emkts')
                ->join('businesses', 'emkts.business_id', '=', 'businesses.id')
                ->join('emkt_senders', 'emkts.emkt_sender_id', '=', 'emkt_senders.id')
                ->join('emkt_templates', 'emkts.emkt_template_id', '=', 'emkt_templates.id')
                ->select('emkts.uuid as emkt_uuid',
                    'emkts.subject as emkt_subject',
                    'emkts.attributes as emkt_attributes',
                    'businesses.name as business_name',
                    'businesses.address as business_address',
                    'emkt_senders.title as emkt_sender_title',
                    'emkt_senders.email as emkt_sender_email',
                    'emkt_templates.slug as emkt_template_slug')
                ->where('emkts.id', '=', $emkt_schedule->emkt_id)
                ->first();

            //Listas de leads
            $emkt_lead_lists = DB::table('emkt_lead_lists')
                ->where('emkt_id', '=', $emkt_schedule->emkt_id)
                ->get();

            foreach ($emkt_lead_lists as $emkt_lead_list) {
                $lead_lists = DB::table('lead_lists')
                    ->where('id', '=', $emkt_lead_list->lead_list_id)
                    ->get();
                //Pegando os leads
                foreach ($lead_lists as $lead_list) {
                    $leads = DB::table('leads')
                        ->where('lead_list_id', '=', $lead_list->id)
                        ->where('status', '=', 1)
                        ->get();
                    foreach ($leads as $lead) {
                        array_push($recipients, ['email' => $lead->email, 'name' => $lead->name]);
                        Mail::to($recipients)->queue(new EmktSendMail($emkt, $lead));
                    }
                }
            }
        }
    }

    public function open($emkt_uuid, $lead_uuid) {
        //insere apenas 1 log por lead
        $log = DB::table('emkt_open_logs')
                ->where('emkt_id', Emkt::where('uuid', $emkt_uuid)->firstOrFail()['id'])
                ->where('lead_id', Lead::where('uuid', $lead_uuid)->firstOrFail()['id'])
                ->first();

        if(empty($log)) {
            $emkt_open_log = DB::table('emkt_open_logs')->insert([
                'uuid' => Str::uuid(),
                'emkt_id' => Emkt::where('uuid', $emkt_uuid)->firstOrFail()['id'],
                'lead_id' => Lead::where('uuid', $lead_uuid)->firstOrFail()['id']
            ]);
        }

        $response = Response::make(File::get("images/blank.png"));
        $response->header('Content-Type', 'image/png');
        return $response;
    }

    public function click($emkt_uuid, $lead_uuid, $link) {
        //insere apenas 1 log por lead
        $log = DB::table('emkt_click_logs')
            ->where('emkt_id', Emkt::where('uuid', $emkt_uuid)->firstOrFail()['id'])
            ->where('lead_id', Lead::where('uuid', $lead_uuid)->firstOrFail()['id'])
            ->where('link', $link)
            ->first();

        if(empty($log)) {
            $emkt_click_log = DB::table('emkt_click_logs')->insert([
                'uuid' => Str::uuid(),
                'link' => $link,
                'emkt_id' => Emkt::where('uuid', $emkt_uuid)->firstOrFail()['id'],
                'lead_id' => Lead::where('uuid', $lead_uuid)->firstOrFail()['id']
            ]);
        }

        return redirect($link);
    }

    public function unsubscribe($lead_uuid) {
        $lead = DB::table('leads')
                ->where('uuid', $lead_uuid)
                ->delete();
        if($lead) {
            $content = 'E-mail removido com sucesso!';
            return view('messages', compact('content'));
        }
        else {
            $content = 'Este e-mail não existe ou ja foi removido. Obrigado!';
            return view('messages', compact('content'));
        }
    }

}
