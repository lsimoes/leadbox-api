<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use App\Models\Lead;
use App\Models\LeadList;
use App\Models\LeadListField;
use App\Models\LeadListFieldValue;
use App\Http\Requests\LeadPostRequest;
use App\Http\Requests\LeadImportPostRequest;

class LeadController extends Controller
{
    public function register(LeadPostRequest $request) {

        $lead_list = LeadList::where('uuid', $request['lead_list_uuid'])->firstOrFail();

        //verifica se este lead ja existe
        $lead_exists = Lead::where('email', $request['email'])->where('lead_list_id', $lead_list['id'])->first();
        empty($lead_exists) ?: abort( response('lead-exists', 404) ); //if not empty, abort

        $lead = DB::table('leads')->insertGetId([
                    'uuid' => Str::uuid(),
                    'name' => $request['name'],
                    'email' => $request['email'],
                    'status' => $request['status'],
                    'lead_list_id' => $lead_list['id'],
                    'business_id' => $lead_list['business_id']
                ]);

        if(isset($request['values'])) {
            $lead_list_fields = LeadListField::where('lead_list_id', $lead_list['id'])->orderBy('id', 'asc')->get();
            $i = 0;
            foreach($lead_list_fields as $field) {
                DB::table('lead_list_field_values')->insert([
                    'uuid' => Str::uuid(),
                    'value' => $request['values'][$i],
                    'lead_list_field_id' => $field->id,
                    'lead_id' => $lead
                ]);
                $i++;
            }
        }
    }

    public function import(LeadImportPostRequest $request) {
        $file = $request->file('file');
        $filename = $file->getClientOriginalName() . '__' . Str::uuid();
        $extension = $file->getClientOriginalExtension();
        Storage::disk('s3')->put('files/leads-imports/'.$filename.'.'.$extension, file_get_contents($file));

        $file_s3 = Storage::disk('s3')->get('files/leads-imports/'.$filename.'.'.$extension);
        $leads = explode("\r\n", $file_s3);

        foreach ($leads as $lead) {

            //separando campos por virgula ou ponto e virgula (tanto faz)
            $lead_values = preg_split( '/(,|;)/', $lead); //(;|#|~)

            //quantidade de campos dos leads
            $count_fields = count($lead_values);

            //se nome e e-mail forem válido
            if(preg_match('/^[a-zA-ZÀ-ÿ\s]+$/', $lead_values[0]) && strlen($lead_values[0]) <= 70 && filter_var($lead_values[1], FILTER_VALIDATE_EMAIL) && strlen($lead_values[1]) <= 70) {
                //insere apenas se o email não existir nesta lista
                $lead_list = LeadList::where('uuid', $request['lead_list_uuid'])->firstOrFail();
                $email_exists = Lead::where('lead_list_id', $lead_list['id'])->where('email', $lead_values[1])->first();
                if(empty($email_exists)) {
                    $lead = DB::table('leads')->insertGetId([
                        'uuid' => Str::uuid(),
                        'name' => $lead_values[0],
                        'email' => $lead_values[1],
                        'status' => 1,
                        'lead_list_id' => $lead_list['id'],
                        'business_id' => $lead_list['business_id']
                    ]);
                }

                //insere os demais valores nos campos adicionais (se tiver) e se a quantidade de campos importados for igual a da tabela
                $lead_list_fields = LeadListField::where('lead_list_id', $lead_list['id'])->orderBy('id', 'asc')->get();

                if(!empty(count($lead_list_fields)) && $count_fields == count($lead_list_fields) + 2) {
                    $i = 2;
                    foreach($lead_list_fields as $field) {
                        $value = DB::table('lead_list_field_values')->insert([
                            'uuid' => Str::uuid(),
                            'value' => $lead_values[$i],
                            'lead_list_field_id' => $field->id,
                            'lead_id' => $lead
                        ]);
                        $i++;
                    }
                }
            }

            //deletando arquivo
            Storage::disk('s3')->delete('files/leads-imports/'.$filename.'.'.$extension);
        }
    }

    public function remove(Request $request) {

        $request['uuids'] ?: abort(404);

        foreach ($request['uuids'] as $uuid)
        {
            $lead = DB::table('leads')
                ->where('uuid', $uuid)
                ->where('business_id', auth()->user()->business_id)
                ->delete();
        }
    }

    public function removeSelected(Request $request) {

        $request['leads'] ?: abort(404);

        foreach ($request['leads'] as $lead)
        {
            $lead = DB::table('leads')
                ->where('uuid', $lead['uuid'])
                ->where('business_id', auth()->user()->business_id)
                ->delete();
        }
    }

    public function list($lead_list_uuid) {
        return Lead::select('id','uuid', 'name', 'email', 'created_at')
                ->where('lead_list_id', LeadList::where('uuid', $lead_list_uuid)->firstOrFail()['id'])
                ->where('business_id', auth()->user()->business_id)
                ->with('children')
                ->get();
    }
}
