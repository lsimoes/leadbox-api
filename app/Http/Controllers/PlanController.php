<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PlanController extends Controller
{
    public function register(Request $request) {
        $plan = DB::table('plans')->insert([
                    'uuid' => Str::uuid(),
                    'title' => $request['title'],
                    'identifier' => $request['identifier'],
                    'stripe_id' => $request['stripe_id'],
                    'leads' => $request['leads'],
                    'users' => $request['users'],
                    'price' => $request['price'],
                ]);
        return $plan;
    }

    public function detail($uuid) {
        $plan = DB::table('plans')
                ->select('uuid', 'title', 'identifier', 'stripe_id', 'leads', 'users', 'price')
                ->where('uuid', $uuid)
                ->get();
        return $plan;
    }

    public function update(Request $request) {
        $plan = DB::table('plans')
              ->where('uuid', $request['uuid'])
              ->update([
                    'title' => $request['title'],
                    'identifier' => $request['identifier'],
                    'stripe_id' => $request['stripe_id'],
                    'leads' => $request['leads'],
                    'users' => $request['users'],
                    'price' => $request['price']
                ]);
        return $plan;
    }

    public function remove(Request $request) {
        $plan = DB::table('plans')->where('uuid', $request['uuid'])->delete();
        return $plan;
    }

    public function list() {
        $plans = DB::table('plans')
                ->select('uuid', 'title', 'identifier', 'stripe_id', 'leads', 'users', 'price')
                ->get();
        return $plans;
    }
}
