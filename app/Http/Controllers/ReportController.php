<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Emkt;

class ReportController extends Controller
{
    public $months_en = array("Feb", "Apr", "May","Aug","Sep","Oct","Dec");
    public $months_pt = array("Fev", "Abr", "Mai","Ago","Set","Out","Dez");

    public function get($type) {
        if($type == 'captured-leads') {
            return $this->capturedLeads();
        }
        else if($type == 'email-rates') {
            return $this->emailRates();
        }
        else if($type == 'views-lps') {
            return $this->viewsLps();
        }
    }

    public function capturedLeads() {
        $arrMonths = array();
        $arrQty = array();
        $arrReport = array();

        $results = DB::table('leads')
        ->selectRaw('year(created_at) year, monthname(created_at) month, count(*) quantity')
        ->groupBy('year', 'month')
        ->orderBy('month', 'desc')
        ->where('business_id', auth()->user()->business_id)
        ->get();

        foreach($results as $result) {
            $month_en = substr($result->month, 0,3);
            $month_pt = str_replace($this->months_en, $this->months_pt, $month_en);

            array_push($arrMonths, $month_pt . "'"  . substr($result->year,2));
            array_push($arrQty, $result->quantity);
        }

        $arrReport = [
            'months' => $arrMonths,
            'quantities' => $arrQty
        ];

        return $arrReport;
    }

    public function emailRates() {
        $arrReport = array();
        $opens = 0;
        $clicks = 0;
        $selectString = 'emkts.id, (select count(1) from emkt_open_logs open where open.emkt_id = emkts.id) as opens, ';
        $selectString = $selectString . '(select count(1) from emkt_click_logs click where click.emkt_id = emkts.id) as clicks ';
        $emkts = Emkt::selectRaw($selectString)->groupBy('emkts.id')->get();

        foreach($emkts as $emkt) {
            $opens += $emkt->opens;
            $clicks += $emkt->clicks;
        }

        $arrReport = [
            'sendings'  => count($emkts),
            'opens' => $opens,
            'clicks' => $clicks
        ];

        return $arrReport;
    }

    public function viewsLps() {
        $arrMonths = array();
        $arrQty = array();
        $arrReport = array();

        $results = DB::table('lp_open_logs')
        ->selectRaw('year(created_at) year, monthname(created_at) month, count(*) quantity')
        ->groupBy('year', 'month')
        ->orderBy('month', 'desc')
        ->where('business_id', auth()->user()->business_id)
        ->get();

        foreach($results as $result) {
            $month_en = substr($result->month, 0,3);
            $month_pt = str_replace($this->months_en, $this->months_pt, $month_en);

            array_push($arrMonths, $month_pt . "'"  . substr($result->year,2));
            array_push($arrQty, $result->quantity);
        }

        $arrReport = [
            'months' => $arrMonths,
            'quantities' => $arrQty
        ];

        return $arrReport;
    }
}
