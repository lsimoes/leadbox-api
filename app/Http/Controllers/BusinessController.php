<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Http\Requests\BusinessPostRequest;

class BusinessController extends Controller
{
    public function register(BusinessPostRequest $request) {
        $business = DB::table('businesses')->insert([
                    'uuid' => Str::uuid(),
                    'name' => $request['name'],
                    'shortname' => $request['shortname'],
                    'docnumber' => $request['docnumber'],
                    'address' => $request['address'],
                    'country' => $request['country'],
                    'state' => $request['state'],
                    'city' => $request['city'],
                    'postalcode' => $request['postalcode'],
                    'status' => $request['status'],
                    'timezone' => $request['timezone'],
                    'plan_id' => $request['plan_id'],
                ]);
        return $business;
    }

    public function detail($uuid) {
        $business = DB::table('businesses')
                ->select('uuid', 'name', 'address', 'status', 'plan_id')
                ->where('uuid', $uuid)
                ->where('user_id', auth()->user()->id)
                ->first();
        return $business;
    }

    public function update(BusinessPostRequest $request) {
        $business = DB::table('businesses')
              ->where('uuid', $request['uuid'])
              ->where('user_id', auth()->user()->id)
              ->update([
                    'name' => $request['name'],
                    'shortname' => $request['shortname'],
                    'docnumber' => $request['docnumber'],
                    'address' => $request['address'],
                    'country' => $request['country'],
                    'state' => $request['state'],
                    'city' => $request['city'],
                    'postalcode' => $request['postalcode'],
                    'status' => $request['status'],
                    'timezone' => $request['timezone'],
                    'plan_id' => $request['plan_id'],
                ]);
        return $business;
    }

    public function remove(Request $request) {
        $business = DB::table('businesses')
            ->where('uuid', $request['uuid'])
            ->where('user_id', auth()->user()->id)
            ->delete();
        return $business;
    }

    public function list() {
        $businesses = DB::table('businesses')
                ->select('uuid', 'name', 'status', 'plan_id')
                ->get();
        return $businesses;
    }
}
