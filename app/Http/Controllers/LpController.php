<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Http\Requests\LpPostRequest;
use App\Models\LpTemplate;
use App\Models\LeadList;
use App\Models\Lp;
use App\Models\Business;

class LpController extends Controller
{
    public function register(LpPostRequest $request) {
        $lp = DB::table('lps')->insertGetId([
            'uuid' => Str::uuid(),
            'title' => $request['title'],
            'slug' => $request['slug'],
            'status' => $request['status'],
            'attributes' => $request['attributes'],
            'lp_template_id' => LpTemplate::where('uuid', $request['lp_template_uuid'])->firstOrFail()['id'],
            'lead_list_id' => LeadList::where('uuid', $request['lead_list_uuid'])->firstOrFail()['id'],
            'user_id' => auth()->user()->id,
            'business_id' => auth()->user()->business_id,
        ]);
    }

    public function detail($uuid) {
        $lp = DB::table('lps')
                ->join('lp_templates', 'lps.lp_template_id', '=', 'lp_templates.id')
                ->join('lead_lists', 'lps.lead_list_id', '=', 'lead_lists.id')
                ->select('lps.uuid as uuid',
                        'lps.title',
                        'lps.slug',
                        'lps.status',
                        'lp_templates.uuid as lp_template_uuid',
                        'lead_lists.uuid as lead_list_uuid')
                ->where('lps.uuid', $uuid)
                ->where('lps.business_id', auth()->user()->business_id)
                ->first();
        return $lp;
    }

    public function constructor($lp_uuid) {
        $lp_template = DB::table('lp_templates')
                ->select('slug')
                ->where('id', Lp::where('uuid', $lp_uuid)->firstOrFail()['lp_template_id'])
                ->first();

        $lp = Lp::where('uuid', $lp_uuid)->firstOrFail();

        $lead_list_fields = DB::table('lead_list_fields')
                ->select('uuid','title','type','options','required')
                ->where('lead_list_id', $lp['lead_list_id'])
                ->where('business_id', auth()->user()->business_id)
                ->get();

        return view('lp_templates.'.$lp_template->slug, compact('lp'));

         //criar os blades dos templates, definir local dos templates no AppServiceProvider
    }

    public function open($shortname, $slug) {

        $lp = DB::table('lps')
                ->join('businesses', 'lps.business_id', '=', 'businesses.id')
                ->join('lp_templates', 'lps.lp_template_id', '=', 'lp_templates.id')
                ->select('lps.id as lp_id',
                        'lps.uuid as lp_uuid',
                        'lps.title as lp_title',
                        'lps.business_id as business_id',
                        'lps.attributes as lp_attributes',
                        'lp_templates.slug as lp_template_slug')
                ->where('lps.business_id', Business::where('shortname', $shortname)->firstOrFail()['id'])
                ->where('lps.slug', $slug)
                ->first();

        $lp_open_log = DB::table('lp_open_logs')->insert([
            'uuid' => Str::uuid(),
            'lp_id' => $lp->lp_id,
            'business_id' => $lp->business_id
        ]);

        return view('lp_templates.'.$lp->lp_template_slug, compact('lp'));
    }

    public function update(LpPostRequest $request) {

        $lp = DB::table('lps')
                ->where('uuid', $request['uuid'])
                ->where('business_id', auth()->user()->business_id)
                ->update([
                        'title' => $request['title'],
                        'slug' => $request['slug'],
                        'attributes' => $request['attributes'],
                        'status' => $request['status'],
                        'lp_template_id' => LpTemplate::where('uuid', $request['lp_template_uuid'])->firstOrFail()['id'],
                        'lead_list_id' => LeadList::where('uuid', $request['lead_list_uuid'])->firstOrFail()['id'],
                    ]);
    }

    public function remove(Request $request) {
        $lp = DB::table('lps')
                ->where('uuid', $request['uuid'])
                ->where('business_id', auth()->user()->business_id)
                ->delete();
    }

    public function list() {
        $lps = DB::table('lps')
                ->join('lead_lists', 'lps.lead_list_id', '=', 'lead_lists.id')
                ->select('lps.uuid as uuid',
                        'lps.title',
                        'lps.slug',
                        'lps.status',
                        'lead_lists.title as lead_list_title')
                ->where('lps.business_id', auth()->user()->business_id)
                ->get();
        return $lps;
    }
}
