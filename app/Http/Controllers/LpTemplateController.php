<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\LpTemplatePostRequest;
use App\Models\LpTemplate;

class LpTemplateController extends Controller
{
    public function register(LpTemplatePostRequest $request) {
        $thumb = $request->file('thumb');
        $thumbname = $request['slug'];
        $extension = $thumb->getClientOriginalExtension();
        Storage::disk('s3')->put('files/thumbs/'.$thumbname.'.'.$extension, file_get_contents($thumb));

        $lp_template = DB::table('lp_templates')->insert([
            'uuid' => Str::uuid(),
            'title' => $request['title'],
            'slug' => $request['slug'],
            'thumb' => $thumbname.'.'.$extension,
            'status' => $request['status']
        ]);
    }

    public function detail($uuid) {
        $lp_template = DB::table('lp_templates')
                ->select('title','slug','thumb','status')
                ->where('uuid', $uuid)
                ->first();
        return $lp_template;
    }

    public function update(LpTemplatePostRequest $request) {
        
        if($request->has('thumb')) {
            $thumb = $request->file('thumb');
            $thumbname = $request['slug'];
            $extension = $thumb->getClientOriginalExtension();
            Storage::disk('s3')->put('files/thumbs/'.$thumbname.'.'.$extension, file_get_contents($thumb));
        }

        $lp_template = DB::table('lp_templates')
              ->where('uuid', $request['uuid'])
              ->update([
                    'title' => $request['title'],
                    'slug' => $request['slug'],
                    'thumb' => $request->has('thumb') ? $thumbname.'.'.$extension : LpTemplate::where('uuid', $request['uuid'])->firstOrFail()['thumb'],
                    'status' => $request['status']
                ]);
    }

    public function list() {
        $lp_templates = DB::table('lp_templates')
                ->select('uuid','title','slug','thumb','status')
                ->get();
        return $lp_templates;
    }
}
