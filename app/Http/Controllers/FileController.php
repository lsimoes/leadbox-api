<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Http\Requests\FilePostRequest;
use App\Models\FileFolder;
use App\Models\File;
use Illuminate\Support\Carbon;

class FileController extends Controller
{
    public function upload(FilePostRequest $request) {
        foreach($request->file('files') as $file) {
            // $file = $request->file('file');
            $nameComplete = $file->getClientOriginalName();
            $name = pathinfo($nameComplete, PATHINFO_FILENAME);
            $slug = Str::slug($name, "-");
            $size = $file->getSize();
            $filename = Str::uuid();
            $extension = $file->getClientOriginalExtension();
            Storage::disk('s3')->put('files/storage/'.$filename.'.'.$extension, file_get_contents($file));

            $file = DB::table('files')->insert([
                'uuid' => Str::uuid(),
                'title' => $nameComplete,
                'slug' => $slug,
                'file' => $filename.'.'.$extension,
                'mime' => $extension,
                'size' => $size,
                'file_folder_id' => FileFolder::where('uuid', $request['file_folder_uuid'])->firstOrFail()['id'],
                'user_id' => auth()->user()->id,
                'business_id' => auth()->user()->business_id,
            ]);
        }
    }

    public function detail($uuid) {
        $arrFile = array();
        $file = DB::table('files')
                ->select('uuid', 'title','slug','file','mime','size')
                ->where('uuid', $uuid)
                ->where('business_id', auth()->user()->business_id)
                ->first();

        $arrFile[] = [
            'uuid' => $file->uuid,
            'title' => $file->title,
            'slug' => $file->slug,
            'mime' => $file->mime,
            'size' => $this->formatSizeUnits($file->size),
            'originalFile' => $file->file,
            'file' => Storage::disk('s3')->temporaryUrl('files/storage/' . $file->file, Carbon::now()->addMinute(),[
                'ResponseContentType' => 'application/octet-stream',
                'ResponseContentDisposition' => 'attachment; filename='.$file->slug.'.'.$file->mime.'',
            ]),
        ];
        return $arrFile;
    }

    public function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

    public function update(FilePostRequest $request) {

        $file = DB::table('files')
                ->where('uuid', $request['uuid'])
                ->update([
                    'title' => $request['title'],
                    'slug' => Str::slug($request['title'], "-"),
                ]);

        // $file_bd = File::where('uuid', $request['uuid'])->firstOrFail();

        // if($request->has('file')) {
        //     Storage::disk('s3')->delete('files/storage/'.$file_bd->file);

        //     $slug = Str::slug($request['title'], "-");
        //     $file = $request->file('file');
        //     $size = $file->getSize();
        //     $filename = $slug;
        //     $extension = $file->getClientOriginalExtension();
        //     Storage::disk('s3')->put('files/storage/'.$filename.'.'.$extension, file_get_contents($file));
        // }

        // $file = DB::table('files')
        //         ->where('uuid', $request['uuid'])
        //         ->update([
        //             'title' => $request['title'],
        //             'slug' => $request->has('file') ? $slug : $file_bd['slug'],
        //             'file' => $request->has('file') ? $filename.'.'.$extension : $file_bd['file'],
        //             'mime' => $request->has('file') ? $extension : $file_bd['mime'],
        //             'size' => $request->has('file') ? $size : $file_bd['size'],
        //             'file_folder_id' => FileFolder::where('uuid', $request['file_folder_uuid'])->firstOrFail()['id'],
        //         ]);
    }

    public function remove(Request $request) {
        $file_bd = File::where('uuid', $request['uuid'])->firstOrFail();
        Storage::disk('s3')->delete('files/storage/'.$file_bd->file);

        $file = DB::table('files')
                ->where('uuid', $request['uuid'])
                ->where('business_id', auth()->user()->business_id)
                ->delete();
    }

    public function list($file_folder_uuid=null) {
        $files = array();
        $getfiles = DB::table('files')
                ->select('uuid','slug','title','file','mime')
                ->when($file_folder_uuid, function ($query) use ($file_folder_uuid) {
                    return $query->where('file_folder_id', FileFolder::where('uuid', $file_folder_uuid)->firstOrFail()['id']);
                })
                ->where('business_id', auth()->user()->business_id)
                ->orderBy('id', 'DESC')
                ->get();

        foreach($getfiles as $file) {
            $files[] = [
                'uuid' => $file->uuid,
                'title' => $file->title,
                'file' => Storage::disk('s3')->temporaryUrl('files/storage/' . $file->file, Carbon::now()->addMinute(),[
                    'ResponseContentType' => 'application/octet-stream',
                    'ResponseContentDisposition' => 'attachment; filename='.$file->slug.'.'.$file->mime.'',
                ]),
                'mime' => $file->mime
            ];
        }
        return $files;
    }

    public function getFile($type, $file) {
        if($type == 'thumb') {
            return response()->make(
                Storage::disk('s3')->get('files/thumbs/' . $file),
                200,
                ['Content-Type' => 'image/jpeg']
            );
        }
    }
}
