<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\EmktTemplatePostRequest;
use App\Models\EmktTemplate;
use Illuminate\Support\Carbon;

class EmktTemplateController extends Controller
{
    public function register(EmktTemplatePostRequest $request) {
        $thumb = $request->file('thumb');
        $thumbname = $request['slug'];
        $extension = $thumb->getClientOriginalExtension();
        Storage::disk('s3')->put('files/thumbs/'.$thumbname.'.'.$extension, file_get_contents($thumb));

        $emkt_template = DB::table('emkt_templates')->insert([
            'uuid' => Str::uuid(),
            'title' => $request['title'],
            'slug' => $request['slug'],
            'thumb' => $thumbname.'.'.$extension,
            'status' => $request['status']
        ]);
    }

    public function detail($uuid) {
        $emkt_template = DB::table('emkt_templates')
                ->select('title','slug','thumb','status')
                ->where('uuid', $uuid)
                ->first();
        return $emkt_template;
    }


    public function update(EmktTemplatePostRequest $request) {

        if($request->has('thumb')) {
            $thumb = $request->file('thumb');
            $thumbname = $request['slug'];
            $extension = $thumb->getClientOriginalExtension();
            Storage::disk('s3')->put('files/thumbs/'.$thumbname.'.'.$extension, file_get_contents($thumb));
        }

        $emkt_template = DB::table('emkt_templates')
              ->where('uuid', $request['uuid'])
              ->update([
                    'title' => $request['title'],
                    'slug' => $request['slug'],
                    'thumb' => $request->has('thumb') ? $thumbname.'.'.$extension : EmktTemplate::where('uuid', $request['uuid'])->firstOrFail()['thumb'],
                    'status' => $request['status']
                ]);
    }

    public function list() {
        $template = array();
        $emkt_templates = DB::table('emkt_templates')
                ->select('uuid','title','slug','thumb','status')
                ->get();
        foreach($emkt_templates as $emkt_template) {
            // $url = Storage::disk('s3')->url('files/thumbs/' . $emkt_template->thumb);
            $template[] = [
                'uuid' => $emkt_template->uuid,
                'title' => $emkt_template->title,
                'thumb' => Storage::disk('s3')->temporaryUrl('files/thumbs/' . $emkt_template->thumb, Carbon::now()->addMinute()),
                'slug' => $emkt_template->slug
            ];
        }
        return $template;
    }

    public function constructor($emkt_template_uuid) {

        $emkt_template = EmktTemplate::where('uuid', $emkt_template_uuid)->firstOrFail();
        return view('emkt_templates.'.$emkt_template->slug, compact('emkt_template'));
    }
}
