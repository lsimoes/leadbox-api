<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Models\Lead;
use App\Models\LeadListField;
use App\Http\Requests\LeadListFieldValuePostRequest;

class LeadListFieldValueController extends Controller
{
    public function register(LeadListFieldValuePostRequest $request) {
        $lead_list_field_value = DB::table('lead_list_field_values')->insert([
                    'uuid' => Str::uuid(),
                    'value' => $request['value'],
                    'lead_list_field_id' => LeadListField::where('uuid', $request['lead_list_field_uuid'])->firstOrFail()['id'],
                    'lead_id' => Lead::where('uuid', $request['lead_uuid'])->firstOrFail()['id']
                ]);
        return $lead_list_field_value;
    }
}
