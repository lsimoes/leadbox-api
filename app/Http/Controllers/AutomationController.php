<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Http\Requests\AutomationPostRequest;
use App\Models\LeadList;
use App\Models\Emkt;
use Carbon\Carbon;
use App\Mail\EmktSendMail;
use Illuminate\Support\Facades\Mail;

class AutomationController extends Controller
{

    private $leads;
    private $now;
    private $linkClicked;

    public function register(AutomationPostRequest $request) {
        $automation = DB::table('automations')->insert([
                    'uuid' => Str::uuid(),
                    'title' => $request['title'],
                    'flow' => $request['flow'],
                    'status' => $request['status'],
                    'business_id' => auth()->user()->business_id,
                    'user_id' => auth()->user()->id
                ]);
    }

    public function detail($uuid) {
        $automation = DB::table('automations')
                ->select('uuid','title','flow','status')
                ->where('uuid', $uuid)
                ->where('business_id', auth()->user()->business_id)
                ->first();
        return $automation;
    }

    public function update(AutomationPostRequest $request) {
        $automation = DB::table('automations')
              ->where('uuid', $request['uuid'])
              ->where('business_id', auth()->user()->business_id)
              ->update([
                    'title' => $request['title'],
                    'flow' => $request['flow'],
                    'status' => $request['status'],
                    'business_id' => auth()->user()->business_id,
                ]);
    }

    public function remove(Request $request) {
        $automation = DB::table('automations')
                ->where('uuid', $request['uuid'])
                ->where('business_id', auth()->user()->business_id)
                ->delete();
    }

    public function list() {
        $automations = DB::table('automations')
                ->select('uuid','title','status')
                ->where('business_id', auth()->user()->business_id)
                ->get();
        return $automations;
    }

    public function proccess(Request $request) {
        $request->ip() == env('ALLOWED_IP_API_CRON') ?: abort( response('no-permission', 403) ); //if not permission, abort

        $this->now = Carbon::now();
        $waitDays = 0;
        $waitHours = 0;
        $waitMinutes = 0;
        $leads = array();

        $automation = DB::table('automations')
                ->first();

        $flow_nodes = json_decode($automation->flow, true);

        // $key = array_search('081d000f-9c79-47be-88b9-5e81c1599e06', array_column($flow_nodes, 'uuid'));
        // return $flow_nodes;

        foreach($flow_nodes as $key => $flow) {

            //Tempo
            if($flow['type'] == 'wait') {
                $waitDays = $flow['days'];
                $waitHours = $flow['hours'];
                $waitMinutes = $flow['minutes'];
            }

            //Leads que entraram na lista
            if($flow['type'] == 'enter-list') {
                $this->enterList($flow['lead_list_uuid'], $waitDays, $waitHours, $waitMinutes);
            }

            //Envia e-mail
            if($flow['type'] == 'send-email') {
                $this->sendEmail($flow['emkt_uuid']);
            }

            //Click em links do email?
            if($flow['type'] == 'click-link') {
                $this->linkClicked = $this->clickLink($flow['emkt_uuid'], $flow['link'], $flow['condition'], $flow['lead_list_uuid']);
            }
        }
    }

    public function enterList($lead_list_uuid, $waitDays, $waitHours, $waitMinutes) {

        //subtrai o wait e tenho o tempo exato que o lead entrou na lista
        $createdAtLead = $this->now->subDays($waitDays)->subHours($waitHours)->subMinutes($waitMinutes)->format("Y-m-d H:i");

        $this->leads = DB::table('leads')
                ->select('uuid','id','name','email')
                ->where('lead_list_id', LeadList::where('uuid', $lead_list_uuid)->firstOrFail()['id'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d %H:%i'))"), "<=",  $createdAtLead)
                ->where('status', '=', 1)
                ->get();
    }

    public function sendEmail($emkt_uuid) {
        if($this->leads != '[]') {
            $recipients = array();

            $emkt = DB::table('emkts')
                ->join('businesses', 'emkts.business_id', '=', 'businesses.id')
                ->join('emkt_senders', 'emkts.emkt_sender_id', '=', 'emkt_senders.id')
                ->join('emkt_templates', 'emkts.emkt_template_id', '=', 'emkt_templates.id')
                ->select('emkts.id as emkt_id',
                    'emkts.uuid as emkt_uuid',
                    'emkts.subject as emkt_subject',
                    'emkts.attributes as emkt_attributes',
                    'businesses.name as business_name',
                    'businesses.address as business_address',
                    'emkt_senders.title as emkt_sender_title',
                    'emkt_senders.email as emkt_sender_email',
                    'emkt_templates.slug as emkt_template_slug')
                ->where('emkts.uuid', '=', $emkt_uuid)
                ->first();

            //Pega os lead (se houverem) e envia email
            foreach ($this->leads as $lead) {
                array_push($recipients, ['email' => $lead->email, 'name' => $lead->name]);
                Mail::to($recipients)->queue(new EmktSendMail($emkt, $lead));
            }
        }
    }

    public function clickLink($emkt_uuid, $link, $condition, $lead_list_uuid) {
        $emkt_id = Emkt::where('uuid', $emkt_uuid)->firstOrFail()['id'];
        $this->leads = array(); //limpo variavel leads
        $leads_ids = array();

        $emkt_click_logs = DB::table('emkt_click_logs')
            ->select('id','lead_id','link')
            ->where('emkt_id', $emkt_id)
            ->get();

        foreach($emkt_click_logs as $emkt_click_log) {
            if($emkt_click_log->link == $link) {
                array_push($leads_ids, $emkt_click_log->lead_id);
            }
        }
        if($condition == 'yes') {
            $this->leads = DB::table('leads')
                ->select('uuid','id','name','email')
                ->where('lead_list_id', LeadList::where('uuid', $lead_list_uuid)->firstOrFail()['id'])
                ->whereIn('id', $leads_ids)
                ->where('status', '=', 1)
                ->get();
        }
        else if($condition == 'no') {
            $this->leads = DB::table('leads')
                ->select('uuid','id','name','email')
                ->where('lead_list_id', LeadList::where('uuid', $lead_list_uuid)->firstOrFail()['id'])
                ->whereNotIn('id', $leads_ids)
                ->where('status', '=', 1)
                ->get();
        }
    }
}
