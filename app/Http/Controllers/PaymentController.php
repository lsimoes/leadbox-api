<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Stripe;

class PaymentController extends Controller
{
    public function __construct()
    {
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
    }

    public function createCustomer(Request $request) {
        Stripe\Customer::create([
            'name' => $request['business_name'],
            'email' => $request['email'],
            'address' => [
                'city' => $request['city'],
                'country' => $request['country'],
                'line1' => $request['line1'],
                'postal_code' => $request['postal_code'],
                'state' => $request['state']
            ],
            'metadata' => [
                'uuid' => $request['uuid'],
                'user_name' => $request['user_name'],
            ],
        ]);
    }

    public function addPaymentMethod(Request $request) {
        //Cria método de pagamanento do ussuário
        $paymentMethod = Stripe\PaymentMethod::create([
            'type' => 'card',
            'card' => [
                'number' => $request['number'],
                'exp_month' => $request['exp_month'],
                'exp_year' => $request['exp_year'],
                'cvc' => $request['cvc'],
            ],
        ]);

        //Anexa o método ao usuário
        $pm = \Stripe\PaymentMethod::retrieve($paymentMethod->id);
        $pm->attach(['customer' => $request['customer_id']]);

        //Seta método como default do usuário
        Stripe\Customer::update(
            $request['customer_id'],
            ['invoice_settings' => ['default_payment_method' => $paymentMethod->id]]
        );

    }

    public function removePaymentMethod(Request $request) {
        //remove o método de pagamento do usuário
        $pm = \Stripe\PaymentMethod::retrieve($request['paymentmethod_id']);
        $pm->detach();
    }

    public function createSubscription(Request $request) {
        Stripe\Subscription::create([
            'customer' => $request['customer_id'],
            'items' => [
                ['price' => $request['price']],
              ],
        ]);
    }

    public function cancelSubscription(Request $request) {
        $sub = \Stripe\Subscription::retrieve($request['subscription_id']);
        $sub->cancel();

        // Stripe\Subscription::update(
        //     $request['subscription_id'],
        //     ['cancel_at_period_end' => true]
        // );
    }

    public function updateSubscription(Request $request) {
        $proration_date = time();

        $subscription = Stripe\Subscription::retrieve($request['subscription_id']);
        Stripe\Subscription::update($request['subscription_id'], [
            'items' => [
                [
                'id' => $subscription->items->data[0]->id,
                'price' => $request['price'],
                ],
            ],
            'proration_date' => $proration_date,
            'proration_behavior' => 'always_invoice'
        ]);
    }

    public function createPortal(Request $request) {
        $session = Stripe\BillingPortal\Session::create([
            'customer' => $request['customer_id'],
            'return_url' => 'https://leadbox.io/account',
          ]);

        return $session;
    }
}
