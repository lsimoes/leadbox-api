<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Http\Requests\FileFolderPostRequest;
use App\Models\FileFolder;
use Illuminate\Support\Facades\Storage;

class FileFolderController extends Controller
{
    public function register(FileFolderPostRequest $request) {
        $file_folder = DB::table('file_folders')->insert([
            'uuid' => Str::uuid(),
            'title' => $request['title'],
            'user_id' => auth()->user()->id,
            'business_id' => auth()->user()->business_id,
        ]);
    }

    public function detail($uuid) {
        $file_folder = DB::table('file_folders')
                ->select('uuid', 'title')
                ->where('uuid', $uuid)
                ->where('business_id', auth()->user()->business_id)
                ->get();
        return $file_folder;
    }

    public function update(FileFolderPostRequest $request) {
        $file_folder = DB::table('file_folders')
              ->where('uuid', $request['uuid'])
              ->where('business_id', auth()->user()->business_id)
              ->update([
                    'title' => $request['title'],
                ]);
    }

    public function remove(Request $request) {

        $files = DB::table('files')
                ->where('file_folder_id', FileFolder::where('uuid', $request['uuid'])->firstOrFail()['id'])
                ->where('business_id', auth()->user()->business_id)
                ->get();
        foreach($files as $file) {
            Storage::disk('s3')->delete('files/storage/'.$file->file);
        }

        $files_db = DB::table('files')
                ->where('file_folder_id', FileFolder::where('uuid', $request['uuid'])->firstOrFail()['id'])
                ->where('business_id', auth()->user()->business_id)
                ->delete();

        $file_folder = DB::table('file_folders')
                ->where('uuid', $request['uuid'])
                ->where('business_id', auth()->user()->business_id)
                ->delete();
    }

    public function list() {
        $file_folders = DB::table('file_folders')
                ->select('uuid', 'title as name')
                ->where('business_id', auth()->user()->business_id)
                ->orderBy('title', 'ASC')
                ->get();
        $arr = array(
            'uuid' => '',
            'name' => 'Todos'
        );

        $file_folders->prepend($arr);

        return $file_folders;
    }

}
