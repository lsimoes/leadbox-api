<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Http\Requests\LeadListPostRequest;
use App\Models\LeadList;
use App\Models\LeadListField;

class LeadListController extends Controller
{
    private $lead_list;
    private $lead_list_field;

    public function __construct(
        LeadList $lead_list,
        LeadListField $lead_list_field
    )
    {
        $this->lead_list = $lead_list;
        $this->lead_list_field = $lead_list_field;
    }

    public function register(LeadListPostRequest $request) {
        $create = $this->lead_list->create([
                    'uuid' => Str::uuid(),
                    'title' => $request['title'],
                    'business_id' => auth()->user()->business_id,
                    'user_id' => auth()->user()->id
                ]);

        foreach($request['fields'] as $field) {
            // return $field['title'];

            if($field['options'] == []) {
                $options = null;
            } else {
                $options = json_encode($field['options'], JSON_UNESCAPED_UNICODE);
            }

            $this->lead_list_field->create([
                'uuid' => Str::uuid(),
                'title' => $field['title'],
                'type' => $field['type'],
                'options' => $options,
                'required' => $field['required'],
                'lead_list_id' => $create->id,
                'business_id' => auth()->user()->business_id,
                'user_id' => auth()->user()->id
            ]);
        }
    }

    public function detail($uuid) {
        return LeadList::select('id', 'uuid', 'title')->where('uuid', $uuid)->with('children')->get();
    }

    public function update(LeadListPostRequest $request) {

        $update = $this->lead_list
            ->where('uuid', $request['uuid'])
            ->where('business_id', auth()->user()->business_id)
            ->update([
                'title' => $request['title']
            ]);

        foreach($request['fields'] as $field) {
            $existField = LeadListField::where('uuid', $field['uuid'])->first();

            if(empty($field['options'])) {
                $options = null;
            } else {
                $options = json_encode($field['options'], JSON_UNESCAPED_UNICODE);
            }

            if($existField) {
                $update = $this->lead_list_field
                    ->where('uuid', $field['uuid'])
                    ->where('business_id', auth()->user()->business_id)
                    ->update([
                        'title' => $field['title'],
                        'type' => $field['type'],
                        'options' => $options,
                        'required' => $field['required']
                    ]);
            }
            else {
                $this->lead_list_field->create([
                    'uuid' => Str::uuid(),
                    'title' => $field['title'],
                    'type' => $field['type'],
                    'options' => $options,
                    'required' => $field['required'],
                    'lead_list_id' => LeadList::where('uuid', $request['uuid'])->firstOrFail()['id'],
                    'business_id' => auth()->user()->business_id,
                    'user_id' => auth()->user()->id
                ]);
            }
        }

        foreach($request['removeds'] as $uuid) {
            $remove = DB::table('lead_list_fields')
                ->where('uuid', $uuid)
                ->where('business_id', auth()->user()->business_id)
                ->delete();
        }
    }

    public function remove(Request $request) {
        $remove = DB::table('lead_lists')
                ->where('uuid', $request['uuid'])
                ->where('business_id', auth()->user()->business_id)
                ->delete();
        return $remove;
    }

    public function list() {
        $list = DB::table('lead_lists')
                ->leftJoin('leads', 'lead_lists.id', '=', 'leads.lead_list_id')
                ->selectRaw('lead_lists.uuid as uuid, lead_lists.title as title, lead_lists.created_at as created_at, count(leads.id) as leads_count')
                ->where('lead_lists.business_id', auth()->user()->business_id)
                ->groupBy('lead_lists.id','lead_lists.uuid','lead_lists.title','lead_lists.created_at')
                ->orderBy('lead_lists.created_at','DESC')
                ->get();
        return $list;
    }
}
