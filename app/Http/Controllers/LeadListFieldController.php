<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Models\LeadList;
use App\Models\LeadListField;
use App\Models\LeadListFieldValue;
use App\Http\Requests\LeadListFieldPostRequest;

class LeadListFieldController extends Controller
{
    public function register(LeadListFieldPostRequest $request) {
        $lead_list_field = DB::table('lead_list_fields')->insert([
                    'uuid' => Str::uuid(),
                    'title' => $request['title'],
                    'type' => $request['type'],
                    'options' => json_encode($request['options'], JSON_UNESCAPED_UNICODE), //salva com acentos
                    'required' => $request['required'],
                    'lead_list_id' => LeadList::where('uuid', $request['lead_list_uuid'])->firstOrFail()['id'],
                    'business_id' => auth()->user()->business_id,
                    'user_id' => auth()->user()->id
                ]);
        return $lead_list_field;
    }

    public function update(LeadListFieldPostRequest $request) {

        //verifica se ja tem algum valor cadastrado, se tiver, não será possível alterar dados 
        $lead_list_field_id = LeadListField::where('uuid', $request['uuid'])->firstOrFail()['id'];

        $value = LeadListFieldValue::where('lead_list_field_id', $lead_list_field_id)->first();

        empty($value) ?: abort(404); //if not empty, abort

        $lead_list_field = DB::table('lead_list_fields')
              ->where('uuid', $request['uuid'])
              ->where('business_id', auth()->user()->business_id)
              ->update([
                    'title' => $request['title'],
                    'type' => $request['type'],
                    'options' => json_encode($request['options'], JSON_UNESCAPED_UNICODE), //salva com acentos
                    'required' => $request['required']
                ]);
        return $lead_list_field;
    }

    public function remove(Request $request) {
        $lead_list_field = DB::table('lead_list_fields')
                ->where('uuid', $request['uuid'])
                ->where('business_id', auth()->user()->business_id)
                ->delete();
        return $lead_list_field;
    }

    public function list($lead_list_uuid) {
        $lead_list_fields = DB::table('lead_list_fields')
                ->select('uuid', 'title','type','options','required')
                ->where('lead_list_id', LeadList::where('uuid', $lead_list_uuid)->firstOrFail()['id'])
                ->where('business_id', auth()->user()->business_id)
                ->get();
        return $lead_list_fields;
    }

}
