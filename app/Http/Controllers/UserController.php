<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UserPostRequest;
use Auth;
use Response;
use App\Models\User;

class UserController extends Controller
{
    public function register(UserPostRequest $request) {
        $user = User::create([
                    'uuid' => Str::uuid(),
                    'is_admin' => $request['is_admin'],
                    'name' => $request['name'],
                    'email' => $request['email'],
                    'password' => Hash::make($request['password']),
                    'status' => $request['status'],
                    'business_id' => $request['business_id'],
                    'user_id' => $request['user_id']
                ]);

        $accessToken = $user->createToken('authToken')->accessToken;
        return response(['access_token' => $accessToken]);
    }

    public function login(Request $request) {
        $auth = Auth::attempt(['email' => $request['email'], 'password' => $request['password']]);

        if ($auth) {
            $user = Auth::user();

            // if ($user->email_verified_at == null) {
            //     return "no_validation";
            // }
            if ($user->status == 0) {
                return Response::json([
                    'error' => 'blocked-account'
                ], 400);
            }
            else
            {
                $accessToken = auth()->user()->createToken('authToken')->accessToken;
                return response(['access_token' => $accessToken]);
            }
        }
        else
        {
            return Response::json([
                'error' => 'invalid-login'
            ], 400);
        }
    }

    public function detail($uuid) {
        $user = DB::table('users')
                ->select('uuid', 'name', 'email')
                ->where('uuid', $uuid)
                ->where('user_id', auth()->user()->id)
                ->get();
        return $user;
    }

    public function update(UserPostRequest $request) {
        $user = DB::table('users')
              ->where('uuid', $request['uuid'])
              ->where('user_id', auth()->user()->id)
              ->update([
                    'name' => $request['name'],
                    'email' => $request['email'],
                    'password' =>  $request->has('password') ? Hash::make($request['password']) : auth()->user()->password, //User::where('uuid', $uuid)->firstOrFail()->get('password')
                    'status' => $request['status'],
                ]);
        return $user;
    }

    public function remove(Request $request) {
        $user = DB::table('users')
                ->where('uuid', $request['uuid'])
                ->where('user_id', auth()->user()->id)
                ->delete();
        return $user;
    }

    public function list() {
        $users = DB::table('users')
                ->select('uuid', 'name', 'status')
                ->where('user_id', auth()->user()->id)
                ->get();
        return $users;
    }
}
